# README #

SQL schema and data for creating the Canary Airline database used in Teach Yourself SQL in 24 Hours.
## Initialize Database ##
Download the schema and data SQL files.
### H2 Database ###
```
java -cp h2-*.jar org.h2.tools.RunScript -url jdbc:h2:./canaryairline -script h2.schema.sql
```
```
java -cp h2-*.jar org.h2.tools.RunScript -url jdbc:h2:./canaryairline -script h2.data.sql
```

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact